## Loon常用配置文件

Loon 是一个界面优美，操作方便的 iOS 网络代理工具，目前支持 `ss`、`ssr`、`vmess` 协议

* ⚠️注:  Loon 不提供具体的代理服务器地址，需自行搭建或者购买机场提供的服务


**懒人场景-我有一个订阅地址，怎么使用 Loon**

第一种：URL 导入方式(通过社群提供的 API)

1. 首先将你的订阅地址通过 urlencode 处理，比如使用这个[地址](http://www.jsons.cn/urlencode/)
2. 将编码后的订阅地址拼接 [`https://loon.now.sh/lazy-config/default?sub=第一步处理后的地址`](https://loon.now.sh/lazy-config/default?sub=%E7%AC%AC%E4%B8%80%E6%AD%A5%E5%A4%84%E7%90%86%E5%90%8E%E7%9A%84%E5%9C%B0%E5%9D%80)
3. 打开配置选择 url 导入，第2步的地址
4. 打开`策略组`，点击一下`🐳节点选择`，可先进行延迟测试，手动选一个较快的节点
5. 将运行模式改成「自动分流」
6. 首页点击 Start 即可享受

该 API 不存在存储订阅地址的行为，单纯只是用于替换下面手动导入配置时的订阅地址，若不放心，可以不加，只导入 [https://loon.now.sh/lazy-config/default](https://loon.now.sh/lazy-config/default) ，这样需要手动编辑订阅地址

第二种：手动导入配置方式

1. 打开[配置文件集合](https://www.notion.so/godtools/aea3efeb1b1e4b38b258c626df09c548)，找到`不折腾版本配置`
2. 将配置文件导入 Loon
3. 导入成功后，打开`配置`->`订阅节点`，左滑编辑 `SubProxy`，将 URL 改成机场的订阅地址，注意`别名`不要变更
4. 打开`策略组`，点击一下`🐳节点选择`，可先进行延迟测试，手动选一个较快的节点
5. 将运行模式改成「自动分流」
6. 首页点击 Start 即可享受